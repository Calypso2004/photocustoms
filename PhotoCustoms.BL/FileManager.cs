﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace PhotoCustoms.BL
{
    public class FileManager
    {
        #region SELECT FILE AND RETURN PATH
        public string SelectFile()
        {
            string path = null;

            OpenFileDialog open_file_dialog = new OpenFileDialog();
            open_file_dialog.Multiselect = false;
            open_file_dialog.FileName = "";
            open_file_dialog.Title = "Выберите файл Excel";
            open_file_dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            open_file_dialog.Filter = "File Excel(*.xls;*.xlsx)|*.xls;*.xlsx|All files(*.*)|*.*";
            open_file_dialog.FilterIndex = 1;
            open_file_dialog.RestoreDirectory = true;

            if (open_file_dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    path = open_file_dialog.FileName.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            return path;
        }
        #endregion

        #region GET ARRAY REFERENCES
        public string GetArrayReferences(string file_path)
        {
            Excel.Application oXl;
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng = null;

            oXl = new Excel.Application();

            oXl.Visible = false;
            //делаем временно неактивным документ
            oXl.Interactive = false;
            oXl.EnableEvents = false;

            oWB = oXl.Workbooks.Open(file_path);
            oSheet = (Excel._Worksheet)oXl.Sheets[1];

            oRng = oSheet.UsedRange;

            DataTable dt = new DataTable();

            dt.Columns.Add("Reference");

            for (int rnum = 19; rnum <= oRng.Rows.Count - 1; rnum++)
            {
                DataRow dr = dt.NewRow();

                dr[0] = (oRng.Cells[rnum, 3] as Excel.Range).Value2;

                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }

            //Показываем ексель
            oXl.Visible = true;

            oXl.Interactive = true;
            oXl.ScreenUpdating = true;
            oXl.UserControl = true;

            oWB.Close();
            oXl.Quit();

            //Отсоединяемся от Excel
            releaseObject(oRng);
            releaseObject(oSheet);
            releaseObject(oWB);
            releaseObject(oXl);

            dt.DefaultView.Sort = "Reference ASC";
            dt = dt.DefaultView.ToTable();
            dt = RemoveDuplicateRows(dt, "Reference");

            string reference = null;

            foreach (DataRow row in dt.Rows)
            {
                reference += row[0].ToString() + ";";
            }

            reference = reference.Remove(reference.Length - 1);

            return reference;
        }
        #endregion
        //перегрузка метода для возврата номера спецификации
        #region GET ARRAY REFERENCES WIHT PARAM OUT
        public string GetArrayReferences(string file_path, out string specification_number)
        {
            Excel.Application oXl;
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng = null;

            oXl = new Excel.Application();

            oXl.Visible = false;
            //делаем временно неактивным документ
            oXl.Interactive = false;
            oXl.EnableEvents = false;

            oWB = oXl.Workbooks.Open(file_path);
            oSheet = (Excel._Worksheet)oXl.Sheets[1];

            oRng = oSheet.UsedRange;

            specification_number = (oRng.Cells[8, 4] as Excel.Range).Value2;

            DataTable dt = new DataTable();

            dt.Columns.Add("Reference");

            for (int rnum = 19; rnum <= oRng.Rows.Count - 1; rnum++)
            {
                DataRow dr = dt.NewRow();

                dr[0] = (oRng.Cells[rnum, 3] as Excel.Range).Value2;

                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }

            //Показываем ексель
            oXl.Visible = true;

            oXl.Interactive = true;
            oXl.ScreenUpdating = true;
            oXl.UserControl = true;

            oWB.Close();
            oXl.Quit();

            //Отсоединяемся от Excel
            releaseObject(oRng);
            releaseObject(oSheet);
            releaseObject(oWB);
            releaseObject(oXl);

            dt.DefaultView.Sort = "Reference ASC";
            dt = dt.DefaultView.ToTable();
            dt = RemoveDuplicateRows(dt, "Reference");

            string reference = null;

            foreach (DataRow row in dt.Rows)
            {
                reference += row[0].ToString() + ";";
            }

            reference = reference.Remove(reference.Length - 1);

            return reference;
        }
        #endregion

        #region REMOVE DUPLICATE ROWS
        public DataTable RemoveDuplicateRows(DataTable table, string DictinctColumn)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[DictinctColumn]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[DictinctColumn]);
                }

                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }
                return table;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        #endregion

        #region ОСВОБОЖДАЕМ РЕСУРЫ (ЗАКРЫВАЕМ eXCEL)
        public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show(ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion

        #region CREATE TEXT FILE
        public void CreateTxtNoReferencePhotoCustoms(string st)
        {
            using (StreamWriter stream = new StreamWriter("D:\\NoReferencePhotoCustoms.txt", true))
            {
                stream.WriteLine(st, Environment.NewLine);
            }
        }

        public void CreateTxtNoTechnicalDocumentations(string st)
        {
            using (StreamWriter stream = new StreamWriter("D:\\NoTechnicalDocumentations.txt", true))
            {
                stream.WriteLine(st, Environment.NewLine);
            }
        }
        #endregion
    }
}
