﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoCustoms.BL
{
    public class TechDocuments
    {
        IMessageService _messageService = new MessageService();

        /// <summary>
        /// Structure technical documentations
        /// </summary>
        /// <param name="input_file">File specification</param>
        /// <param name="out_path">Source technical documentations</param>
        /// <param name="in_path">Path structure technical documentations</param>
        public void StructureTechDoc(string input_file, string out_path, string in_path)
        {
            string specification_number = "";

            FileManager fm = new FileManager();
            string list_of_articles = fm.GetArrayReferences(input_file,out specification_number);

            string[] listArticles = list_of_articles.Split(new Char[] { ';' });

            bool no_tech_doc = false;

            string foldername = in_path + "\\" + specification_number;

            foreach (string article in listArticles)
            {
                string[] fileEntries = Directory.GetFiles(out_path, "*.PDF", SearchOption.TopDirectoryOnly);

                foreach (string fileName in fileEntries)
                {
                    string fname = Path.GetFileName(fileName);

                    if (fname.Contains(article))
                    {
                        if (!Directory.Exists(foldername)) Directory.CreateDirectory(foldername);
                        try
                        {
                            File.Copy(fileName, foldername + "\\" + fname, true);

                            no_tech_doc = false;
                            break;
                        }
                        catch (Exception ex)
                        {
                            _messageService.ShowError(ex.Message);
                        }  
                    }
                    else
                    {
                        no_tech_doc = true;
                    }
                }

                if (no_tech_doc == true)
                {
                    // записываем отсутствующий артикул в текстовый файл
                    fm.CreateTxtNoTechnicalDocumentations(article);
                }
            }
        }
    }
}
