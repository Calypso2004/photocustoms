﻿using System;
using System.Windows.Forms;

using PhotoCustoms.BL;

namespace PhotoCustoms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        //    Image a = Image.FromFile("D://test.jpg");

        //    int x = a.Width/2; int y = a.Height/2;

        //    Graphics part2 = Graphics.FromImage(a);
        //    part2.DrawString("01/01/2016 09:00",
        //            new System.Drawing.Font("Arial", 20, FontStyle.Bold),
        //            new SolidBrush(Color.Orange), new RectangleF(x, y, 0, 500),
        //            new StringFormat(StringFormatFlags.NoWrap));
        //    a.Save("D://imgres.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            FileManager fm = new FileManager();
            string path = fm.SelectFile();

            fldText.Text = fm.GetArrayReferences(path);
        }
    }
}
