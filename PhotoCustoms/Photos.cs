﻿using System;
using System.Drawing;
using System.IO;
using PhotoCustoms.BL;
using System.Drawing.Drawing2D;

namespace PhotoCustoms
{
    class Photos
    {
        IMessageService _messageService = new MessageService();

        public float font_size = 10;
        public string font_family = "Arial";
        public string photo_path = "";
        public string temp_output_path = "";

        //Return a path of output image
        private void PhotoCreator(string in_path, string out_path, string dt)
        {
            //in_path = "D://test.jpg"
            Image source_image = Image.FromFile(in_path);

            //set scale image 333x249
            source_image = ScaleImage(source_image);

            int x = source_image.Width; int y = source_image.Height;

            using (Graphics return_image = Graphics.FromImage(source_image))
            {
                // Set world transform of graphics object to translate.
                return_image.TranslateTransform(20.0f, 0.0f);

                // Then to rotate, prepending rotation matrix.
                return_image.RotateTransform(90.0F);

                return_image.DrawString(dt,
                        new System.Drawing.Font(font_family, font_size, FontStyle.Bold),
                        // Text in right down angle
                        //new SolidBrush(Color.Orange), new RectangleF(900, 1950, 0, 500),
                        new SolidBrush(Color.Orange), new RectangleF(x - 30, 0, 0, 0),
                        new StringFormat(StringFormatFlags.NoWrap));                
            }
            //out_path = "D://imgres.jpg";
            source_image.Save(out_path, System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        //Return a new path with updated photos
        public void findPhotoPath(string article, DateTime dt, bool datetimeoff)
        {
            string dt_snapshot = "";
            //datetimeoff - switch off datetime into the photos
            if (datetimeoff)
            {
                dt_snapshot = dt.ToString("d:MM:yyyy HH:mm");
            }

            bool DirectoryExists = Directory.Exists(photo_path + "\\" + article);

            if (DirectoryExists == true)
            {
                string[] fileEntries = Directory.GetFiles(photo_path + "\\" + article);
                foreach (string fileName in fileEntries)
                {
                    string out_file = temp_output_path + "\\" + Path.GetFileName(fileName);

                    if(Path.GetExtension(fileName) == ".JPG" || Path.GetExtension(fileName) == ".jpg")
                    {
                        PhotoCreator(fileName, out_file, dt_snapshot);
                    }
                    //File.Copy(fileName, out_file);
                }
            }
            else
            {
                // записываем отсутствующий артикул в текстовый файл
                FileManager fm = new FileManager();
                fm.CreateTxtNoReferencePhotoCustoms(article);
            }
        }

        //Make structure of the path
        public void StructurePath(string path)
        {
            string current_article = "";
            
            string[] fileEntries = Directory.GetFiles(path, "*.JPG", SearchOption.TopDirectoryOnly);
            foreach (string fileName in fileEntries)
            {
                string fname = Path.GetFileName(fileName);

                current_article = fname.Substring(0, fname.Trim().IndexOf("("));
                string foldername = path + "\\" + current_article;

                if (!Directory.Exists(foldername)) Directory.CreateDirectory(foldername);

                try
                {
                    File.Copy(fileName, foldername + "\\" + fname, true);
                }
                catch (Exception ex)
                {
                    _messageService.ShowError(ex.Message);
                }
            }
        }

        //Return scale image
        public Image ScaleImage(Image source_image)
        {
            int width = source_image.Width;
            int height = source_image.Height;
            int big_aspect = 333;
            int newWidth;
            int newHeight;

            if (height > width)
            {
                newWidth = width * big_aspect / height;
                newHeight = big_aspect;
            }
            else
            {
                newHeight = height * big_aspect / width;
                newWidth = big_aspect;
            }

            Image new_source_image = new Bitmap(newWidth, newHeight);

            using (Graphics gr = Graphics.FromImage(new_source_image))
            {
                gr.CompositingQuality = CompositingQuality.HighQuality;
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(source_image, 0, 0, newWidth, newHeight);
                //Закомментировал так как вылетает предупреждение при анализе кода "CA2202: не удаляйте объекты несколько раз"
                //gr.Dispose();
            }

            return new_source_image;
        }

        //Расскидываем фото артикулов по спецификациям
        public void StructurePhotoBySpecification(string input_file, string out_path, string in_path)
        {
            string specification_number = "";

            FileManager fm = new FileManager();
            string list_of_articles = fm.GetArrayReferences(input_file, out specification_number);

            string[] listArticles = list_of_articles.Split(new Char[] { ';' });

            string foldername = in_path + "\\" + specification_number;

            string[] fileEntries = Directory.GetFiles(out_path, "*.JPG", SearchOption.AllDirectories);

            foreach (string article in listArticles)
            {
                foreach (string fileName in fileEntries)
                {
                    string fname = Path.GetFileName(fileName);

                    if (fname.Contains(article))
                    {
                        if (!Directory.Exists(foldername)) Directory.CreateDirectory(foldername);
                        try
                        {
                            File.Copy(fileName, foldername + "\\" + fname, true);
                        }
                        catch (Exception ex)
                        {
                            _messageService.ShowError(ex.Message);
                        }
                    }
                }
            }
        }
    }
}
