﻿using System;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using PhotoCustoms.BL;

namespace PhotoCustoms
{
    class Report
    {
        IMessageService _messageService = new MessageService();

        public void CreateExcelReport(string photo_path)
        {
            Excel.Application xlApp = new Excel.Application();
            xlApp.DisplayAlerts = false;
            xlApp.Visible = false;
            try
            {
                Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
                Excel.Worksheet xlWorkSheet = xlWorkBook.Worksheets[1];
                xlWorkSheet.Name = "Result";

                //xlWorkSheet.Columns.ColumnWidth = 275; xlWorkSheet.Rows.RowHeight = 275;
                //xlWorkSheet.Columns["A:C"].ColumnWidth = 275; xlWorkSheet.Rows["1:100"].RowHeight = 275;

                Excel.Range rng = xlWorkSheet.get_Range("A:C", System.Type.Missing);

                rng.EntireColumn.ColumnWidth = 35.5F;
                rng.RowHeight = 250;

                float r = 0; int c = 2; int i = 1; int rb = 4;
                string[] fileEntries = Directory.GetFiles(photo_path);
                foreach (string fileName in fileEntries)
                {
                     //= Excel.XlPlacement.xlFreeFloating;
                    var shape = xlWorkSheet.Shapes.AddPicture(fileName, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, c, r, 187.5F, 250);
                    shape.Placement = Excel.XlPlacement.xlFreeFloating;

                    c = c + 190;
                    if (c > 570)
                    {
                        c = 2; r = r + 249.75F; i++;
                        //insert horizontal page breaks
                        xlWorkSheet.HPageBreaks.Add(xlWorkSheet.Range["A"+rb]);
                        rb = rb + 3;
                    }
                }
                //insert vertical page breaks and othe propertie page setup
                xlWorkSheet.VPageBreaks.Add(xlWorkSheet.Range["D1"]);
                xlWorkSheet.PageSetup.Zoom = false;
                xlWorkSheet.PageSetup.FitToPagesWide = 1;
                xlWorkSheet.PageSetup.FitToPagesTall = false;
                xlWorkSheet.PageSetup.CenterHorizontally = true;
                xlWorkSheet.PageSetup.CenterVertically = false;

                //rng = xlWorkSheet.get_Range("A1:C"+i.ToString(), System.Type.Missing);
                //rng.Borders.ColorIndex = 1;

                //xlWorkBook.Save();
                xlApp.DisplayAlerts = true;
                xlApp.Visible = true;
                //xlApp.Quit();
                xlApp = null;

            }
            catch(SystemException ex)
            {
                _messageService.ShowError(ex.Message);

                xlApp.DisplayAlerts = true;
                xlApp.Visible = true;
                xlApp.Quit();
                xlApp = null;
            }
        }
    }
}
