﻿namespace PhotoCustoms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.CreateImageReport = new System.Windows.Forms.Button();
            this.InputPath = new System.Windows.Forms.TextBox();
            this.GetInputPath = new System.Windows.Forms.Button();
            this.InputPathLabel = new System.Windows.Forms.Label();
            this.GlobalPathLabel = new System.Windows.Forms.Label();
            this.GetPhotoBasePath = new System.Windows.Forms.Button();
            this.PhotoBasePath = new System.Windows.Forms.TextBox();
            this.StructureButton = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.dtCalendar = new System.Windows.Forms.DateTimePicker();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.buttonOpenFileTxt = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.checkDateOff = new System.Windows.Forms.CheckBox();
            this.gbTechDoc = new System.Windows.Forms.GroupBox();
            this.btnOpenTxtNoTechnicalDocumentations = new System.Windows.Forms.Button();
            this.btStructureTechDoc = new System.Windows.Forms.Button();
            this.btTechDocSpec = new System.Windows.Forms.Button();
            this.btGetTechDocSource = new System.Windows.Forms.Button();
            this.txtTechDocSpec = new System.Windows.Forms.TextBox();
            this.txtTechDocSource = new System.Windows.Forms.TextBox();
            this.lblTechDocSource = new System.Windows.Forms.Label();
            this.lblTechDocSpec = new System.Windows.Forms.Label();
            this.gbPhotoAct = new System.Windows.Forms.GroupBox();
            this.butStructurePhotoBySpecification = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.tsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDefaultSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1.SuspendLayout();
            this.gbTechDoc.SuspendLayout();
            this.gbPhotoAct.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // CreateImageReport
            // 
            this.CreateImageReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreateImageReport.Location = new System.Drawing.Point(37, 204);
            this.CreateImageReport.Name = "CreateImageReport";
            this.CreateImageReport.Size = new System.Drawing.Size(118, 50);
            this.CreateImageReport.TabIndex = 0;
            this.CreateImageReport.Text = "Сформировать";
            this.toolTip1.SetToolTip(this.CreateImageReport, "Сформировать Акт");
            this.CreateImageReport.UseVisualStyleBackColor = true;
            this.CreateImageReport.Click += new System.EventHandler(this.CreateImageReport_Click);
            // 
            // InputPath
            // 
            this.InputPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.InputPath.Location = new System.Drawing.Point(62, 135);
            this.InputPath.Name = "InputPath";
            this.InputPath.ReadOnly = true;
            this.InputPath.Size = new System.Drawing.Size(446, 20);
            this.InputPath.TabIndex = 1;
            // 
            // GetInputPath
            // 
            this.GetInputPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetInputPath.Image = global::PhotoCustoms.Properties.Resources.edit;
            this.GetInputPath.Location = new System.Drawing.Point(489, 101);
            this.GetInputPath.Name = "GetInputPath";
            this.GetInputPath.Size = new System.Drawing.Size(38, 36);
            this.GetInputPath.TabIndex = 2;
            this.toolTip1.SetToolTip(this.GetInputPath, "Выбрать спецификацию");
            this.GetInputPath.UseVisualStyleBackColor = true;
            this.GetInputPath.Click += new System.EventHandler(this.GetInputPath_Click);
            // 
            // InputPathLabel
            // 
            this.InputPathLabel.AutoSize = true;
            this.InputPathLabel.Location = new System.Drawing.Point(59, 116);
            this.InputPathLabel.Name = "InputPathLabel";
            this.InputPathLabel.Size = new System.Drawing.Size(117, 13);
            this.InputPathLabel.TabIndex = 3;
            this.InputPathLabel.Text = "Путь к спецификации";
            // 
            // GlobalPathLabel
            // 
            this.GlobalPathLabel.AutoSize = true;
            this.GlobalPathLabel.Location = new System.Drawing.Point(60, 173);
            this.GlobalPathLabel.Name = "GlobalPathLabel";
            this.GlobalPathLabel.Size = new System.Drawing.Size(92, 13);
            this.GlobalPathLabel.TabIndex = 6;
            this.GlobalPathLabel.Text = "Путь к фотобазе";
            // 
            // GetPhotoBasePath
            // 
            this.GetPhotoBasePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetPhotoBasePath.Image = global::PhotoCustoms.Properties.Resources.edit;
            this.GetPhotoBasePath.Location = new System.Drawing.Point(489, 158);
            this.GetPhotoBasePath.Name = "GetPhotoBasePath";
            this.GetPhotoBasePath.Size = new System.Drawing.Size(38, 36);
            this.GetPhotoBasePath.TabIndex = 5;
            this.toolTip1.SetToolTip(this.GetPhotoBasePath, "Выбрать папку с фотобазой");
            this.GetPhotoBasePath.UseVisualStyleBackColor = true;
            this.GetPhotoBasePath.Click += new System.EventHandler(this.GetPhotoBasePath_Click);
            // 
            // PhotoBasePath
            // 
            this.PhotoBasePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PhotoBasePath.Location = new System.Drawing.Point(62, 192);
            this.PhotoBasePath.Name = "PhotoBasePath";
            this.PhotoBasePath.ReadOnly = true;
            this.PhotoBasePath.Size = new System.Drawing.Size(446, 20);
            this.PhotoBasePath.TabIndex = 4;
            // 
            // StructureButton
            // 
            this.StructureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StructureButton.Location = new System.Drawing.Point(161, 204);
            this.StructureButton.Name = "StructureButton";
            this.StructureButton.Size = new System.Drawing.Size(118, 50);
            this.StructureButton.TabIndex = 7;
            this.StructureButton.Text = "Структура";
            this.toolTip1.SetToolTip(this.StructureButton, "Структурировать фото артикулов по папкам с названием артикула");
            this.StructureButton.UseVisualStyleBackColor = true;
            this.StructureButton.Click += new System.EventHandler(this.StructureButton_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(539, 9);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(61, 13);
            this.lblVersion.TabIndex = 8;
            this.lblVersion.Text = "ver. 0.0.0.0";
            // 
            // dtCalendar
            // 
            this.dtCalendar.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtCalendar.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCalendar.Location = new System.Drawing.Point(61, 75);
            this.dtCalendar.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.dtCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtCalendar.Name = "dtCalendar";
            this.dtCalendar.Size = new System.Drawing.Size(151, 20);
            this.dtCalendar.TabIndex = 9;
            this.toolTip1.SetToolTip(this.dtCalendar, "Дата первого снимка");
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(60, 57);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(118, 13);
            this.lblDateTime.TabIndex = 10;
            this.lblDateTime.Text = "Дата первого снимка";
            // 
            // buttonOpenFileTxt
            // 
            this.buttonOpenFileTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOpenFileTxt.Location = new System.Drawing.Point(409, 204);
            this.buttonOpenFileTxt.Name = "buttonOpenFileTxt";
            this.buttonOpenFileTxt.Size = new System.Drawing.Size(118, 50);
            this.buttonOpenFileTxt.TabIndex = 11;
            this.buttonOpenFileTxt.Text = "Лог файл";
            this.toolTip1.SetToolTip(this.buttonOpenFileTxt, "Открыть лог файл с артикулами для которых нет фото\r\n");
            this.buttonOpenFileTxt.UseVisualStyleBackColor = true;
            this.buttonOpenFileTxt.Click += new System.EventHandler(this.butFileOpen_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblStatusText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 502);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(612, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(118, 17);
            this.lblStatus.Text = "Статус выполнения:";
            // 
            // lblStatusText
            // 
            this.lblStatusText.Name = "lblStatusText";
            this.lblStatusText.Size = new System.Drawing.Size(0, 17);
            // 
            // checkDateOff
            // 
            this.checkDateOff.AutoSize = true;
            this.checkDateOff.Location = new System.Drawing.Point(218, 78);
            this.checkDateOff.Name = "checkDateOff";
            this.checkDateOff.Size = new System.Drawing.Size(106, 17);
            this.checkDateOff.TabIndex = 13;
            this.checkDateOff.Text = "Отключить дату";
            this.toolTip1.SetToolTip(this.checkDateOff, "Не выводить дату на фото артикула");
            this.checkDateOff.UseVisualStyleBackColor = true;
            this.checkDateOff.CheckedChanged += new System.EventHandler(this.checkDateOff_CheckedChanged);
            // 
            // gbTechDoc
            // 
            this.gbTechDoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTechDoc.Controls.Add(this.btnOpenTxtNoTechnicalDocumentations);
            this.gbTechDoc.Controls.Add(this.btStructureTechDoc);
            this.gbTechDoc.Controls.Add(this.btTechDocSpec);
            this.gbTechDoc.Controls.Add(this.btGetTechDocSource);
            this.gbTechDoc.Controls.Add(this.txtTechDocSpec);
            this.gbTechDoc.Controls.Add(this.txtTechDocSource);
            this.gbTechDoc.Controls.Add(this.lblTechDocSource);
            this.gbTechDoc.Controls.Add(this.lblTechDocSpec);
            this.gbTechDoc.Location = new System.Drawing.Point(25, 304);
            this.gbTechDoc.Name = "gbTechDoc";
            this.gbTechDoc.Size = new System.Drawing.Size(565, 195);
            this.gbTechDoc.TabIndex = 14;
            this.gbTechDoc.TabStop = false;
            this.gbTechDoc.Text = "Техническая документация";
            // 
            // btnOpenTxtNoTechnicalDocumentations
            // 
            this.btnOpenTxtNoTechnicalDocumentations.Location = new System.Drawing.Point(169, 133);
            this.btnOpenTxtNoTechnicalDocumentations.Name = "btnOpenTxtNoTechnicalDocumentations";
            this.btnOpenTxtNoTechnicalDocumentations.Size = new System.Drawing.Size(125, 50);
            this.btnOpenTxtNoTechnicalDocumentations.TabIndex = 21;
            this.btnOpenTxtNoTechnicalDocumentations.Text = "Лог файл";
            this.toolTip1.SetToolTip(this.btnOpenTxtNoTechnicalDocumentations, "Открыть лог файл с номерами тех. документаций для которых нет тех. документаций\r\n" +
        "");
            this.btnOpenTxtNoTechnicalDocumentations.UseVisualStyleBackColor = true;
            this.btnOpenTxtNoTechnicalDocumentations.Click += new System.EventHandler(this.btnOpenTxtNoTechnicalDocumentations_Click);
            // 
            // btStructureTechDoc
            // 
            this.btStructureTechDoc.Location = new System.Drawing.Point(38, 133);
            this.btStructureTechDoc.Name = "btStructureTechDoc";
            this.btStructureTechDoc.Size = new System.Drawing.Size(125, 50);
            this.btStructureTechDoc.TabIndex = 16;
            this.btStructureTechDoc.Text = "Структура";
            this.toolTip1.SetToolTip(this.btStructureTechDoc, "Структурировать тех. документации по папкам с номером спецификации");
            this.btStructureTechDoc.UseVisualStyleBackColor = true;
            this.btStructureTechDoc.Click += new System.EventHandler(this.btStructureTechDoc_Click);
            // 
            // btTechDocSpec
            // 
            this.btTechDocSpec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btTechDocSpec.Image = global::PhotoCustoms.Properties.Resources.edit;
            this.btTechDocSpec.Location = new System.Drawing.Point(489, 92);
            this.btTechDocSpec.Name = "btTechDocSpec";
            this.btTechDocSpec.Size = new System.Drawing.Size(38, 36);
            this.btTechDocSpec.TabIndex = 16;
            this.toolTip1.SetToolTip(this.btTechDocSpec, "Выбрать папку для храненения тех. документаций по спецификациям");
            this.btTechDocSpec.UseVisualStyleBackColor = true;
            this.btTechDocSpec.Click += new System.EventHandler(this.btTechDocSpec_Click);
            // 
            // btGetTechDocSource
            // 
            this.btGetTechDocSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btGetTechDocSource.Image = global::PhotoCustoms.Properties.Resources.edit;
            this.btGetTechDocSource.Location = new System.Drawing.Point(489, 44);
            this.btGetTechDocSource.Name = "btGetTechDocSource";
            this.btGetTechDocSource.Size = new System.Drawing.Size(38, 36);
            this.btGetTechDocSource.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btGetTechDocSource, "Выбрать папку с тех. документациями");
            this.btGetTechDocSource.UseVisualStyleBackColor = true;
            this.btGetTechDocSource.Click += new System.EventHandler(this.btGetTechDocSource_Click);
            // 
            // txtTechDocSpec
            // 
            this.txtTechDocSpec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTechDocSpec.Location = new System.Drawing.Point(38, 101);
            this.txtTechDocSpec.Name = "txtTechDocSpec";
            this.txtTechDocSpec.ReadOnly = true;
            this.txtTechDocSpec.Size = new System.Drawing.Size(445, 20);
            this.txtTechDocSpec.TabIndex = 19;
            // 
            // txtTechDocSource
            // 
            this.txtTechDocSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTechDocSource.Location = new System.Drawing.Point(38, 53);
            this.txtTechDocSource.Name = "txtTechDocSource";
            this.txtTechDocSource.ReadOnly = true;
            this.txtTechDocSource.Size = new System.Drawing.Size(445, 20);
            this.txtTechDocSource.TabIndex = 18;
            // 
            // lblTechDocSource
            // 
            this.lblTechDocSource.AutoSize = true;
            this.lblTechDocSource.Location = new System.Drawing.Point(35, 37);
            this.lblTechDocSource.Name = "lblTechDocSource";
            this.lblTechDocSource.Size = new System.Drawing.Size(152, 13);
            this.lblTechDocSource.TabIndex = 16;
            this.lblTechDocSource.Text = "Источник тех. документаций";
            // 
            // lblTechDocSpec
            // 
            this.lblTechDocSpec.AutoSize = true;
            this.lblTechDocSpec.Location = new System.Drawing.Point(35, 85);
            this.lblTechDocSpec.Name = "lblTechDocSpec";
            this.lblTechDocSpec.Size = new System.Drawing.Size(203, 13);
            this.lblTechDocSpec.TabIndex = 17;
            this.lblTechDocSpec.Text = "Тех. документации по спецификациям";
            // 
            // gbPhotoAct
            // 
            this.gbPhotoAct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPhotoAct.Controls.Add(this.butStructurePhotoBySpecification);
            this.gbPhotoAct.Controls.Add(this.buttonOpenFileTxt);
            this.gbPhotoAct.Controls.Add(this.CreateImageReport);
            this.gbPhotoAct.Controls.Add(this.StructureButton);
            this.gbPhotoAct.Controls.Add(this.GetInputPath);
            this.gbPhotoAct.Controls.Add(this.GetPhotoBasePath);
            this.gbPhotoAct.Location = new System.Drawing.Point(25, 25);
            this.gbPhotoAct.Name = "gbPhotoAct";
            this.gbPhotoAct.Size = new System.Drawing.Size(565, 273);
            this.gbPhotoAct.TabIndex = 15;
            this.gbPhotoAct.TabStop = false;
            this.gbPhotoAct.Text = "Печать фото актов";
            // 
            // butStructurePhotoBySpecification
            // 
            this.butStructurePhotoBySpecification.Location = new System.Drawing.Point(285, 204);
            this.butStructurePhotoBySpecification.Name = "butStructurePhotoBySpecification";
            this.butStructurePhotoBySpecification.Size = new System.Drawing.Size(118, 50);
            this.butStructurePhotoBySpecification.TabIndex = 17;
            this.butStructurePhotoBySpecification.Text = "Структура фото \r\nпо спецификациям";
            this.toolTip1.SetToolTip(this.butStructurePhotoBySpecification, resources.GetString("butStructurePhotoBySpecification.ToolTip"));
            this.butStructurePhotoBySpecification.UseVisualStyleBackColor = true;
            this.butStructurePhotoBySpecification.Click += new System.EventHandler(this.butStructurePhotoBySpecification_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(612, 24);
            this.menuStrip.TabIndex = 16;
            this.menuStrip.Text = "menuStrip";
            // 
            // tsMenu
            // 
            this.tsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSettings});
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.Size = new System.Drawing.Size(53, 20);
            this.tsMenu.Text = "Меню";
            // 
            // tsSettings
            // 
            this.tsSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDefaultSettings});
            this.tsSettings.Name = "tsSettings";
            this.tsSettings.Size = new System.Drawing.Size(134, 22);
            this.tsSettings.Text = "Настройки";
            // 
            // tsDefaultSettings
            // 
            this.tsDefaultSettings.Name = "tsDefaultSettings";
            this.tsDefaultSettings.Size = new System.Drawing.Size(224, 22);
            this.tsDefaultSettings.Text = "Параметры по умолчанию";
            this.tsDefaultSettings.Click += new System.EventHandler(this.tsDefaultSettings_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 524);
            this.Controls.Add(this.gbTechDoc);
            this.Controls.Add(this.checkDateOff);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblDateTime);
            this.Controls.Add(this.dtCalendar);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.GlobalPathLabel);
            this.Controls.Add(this.PhotoBasePath);
            this.Controls.Add(this.InputPathLabel);
            this.Controls.Add(this.InputPath);
            this.Controls.Add(this.gbPhotoAct);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Main";
            this.Text = "PhotoCustoms";
            this.Load += new System.EventHandler(this.Main_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gbTechDoc.ResumeLayout(false);
            this.gbTechDoc.PerformLayout();
            this.gbPhotoAct.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateImageReport;
        private System.Windows.Forms.TextBox InputPath;
        private System.Windows.Forms.Button GetInputPath;
        private System.Windows.Forms.Label InputPathLabel;
        private System.Windows.Forms.Label GlobalPathLabel;
        private System.Windows.Forms.Button GetPhotoBasePath;
        private System.Windows.Forms.TextBox PhotoBasePath;
        private System.Windows.Forms.Button StructureButton;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.DateTimePicker dtCalendar;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Button buttonOpenFileTxt;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusText;
        private System.Windows.Forms.CheckBox checkDateOff;
        private System.Windows.Forms.GroupBox gbTechDoc;
        private System.Windows.Forms.GroupBox gbPhotoAct;
        private System.Windows.Forms.Button btnOpenTxtNoTechnicalDocumentations;
        private System.Windows.Forms.Button btStructureTechDoc;
        private System.Windows.Forms.Button btTechDocSpec;
        private System.Windows.Forms.Button btGetTechDocSource;
        private System.Windows.Forms.TextBox txtTechDocSpec;
        private System.Windows.Forms.TextBox txtTechDocSource;
        private System.Windows.Forms.Label lblTechDocSource;
        private System.Windows.Forms.Label lblTechDocSpec;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsMenu;
        private System.Windows.Forms.ToolStripMenuItem tsSettings;
        private System.Windows.Forms.ToolStripMenuItem tsDefaultSettings;
        private System.Windows.Forms.Button butStructurePhotoBySpecification;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

