﻿using System;
using System.IO;
using System.Windows.Forms;

using PhotoCustoms.BL;
using System.Deployment.Application;
using System.Diagnostics;

namespace PhotoCustoms
{
    public partial class Main : Form
    {
        IMessageService _messageService = new MessageService();

        public Main()
        {
            InitializeComponent();
        }

        //Load the Main Form
        private void Main_Load(object sender, EventArgs e)
        {
            //Параметры с формы
            PhotoBasePath.Text = Properties.Settings.Default.PhotoPath;
            InputPath.Text = Properties.Settings.Default.ChoosePath;

            txtTechDocSource.Text = Properties.Settings.Default.OutPathTechDoc;
            txtTechDocSpec.Text = Properties.Settings.Default.InPathTechDoc;

            // версия компиляции
            Version ver = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            lblVersion.Text = "ver. " + ver.ToString();
        }

        //Change the Input Excel File path
        private void GetInputPath_Click(object sender, EventArgs e)
        {
            FileManager fm = new FileManager();

            string select_file_path = fm.SelectFile();

            if (select_file_path != null)
            {
                InputPath.Text = select_file_path;
                Properties.Settings.Default.ChoosePath = select_file_path;
                Properties.Settings.Default.Save();
            }
        }

        //Main Function
        private void CreatePhotos(string photo_path, string input_file, DateTime dt, bool datetimeoff)
        {
            //Get array references
            FileManager fm = new FileManager();
            string list_of_articles = fm.GetArrayReferences(input_file);

            string new_temp_path = photo_path + "\\" + Guid.NewGuid().ToString();
            //Generate a Report
            string[] arr = list_of_articles.Split(new Char[] { ';' });

            //Create an Instance of Photos Class 
            Photos ph = new Photos();
            ph.photo_path = photo_path;

            if (!Directory.Exists(new_temp_path)) Directory.CreateDirectory(new_temp_path);
            ph.temp_output_path = new_temp_path;

            //Get all photos
            foreach (string article in arr)
            {
                ph.findPhotoPath(article, dt, datetimeoff);
                dt = dt.AddMinutes(1d);
            }

            Report r = new Report();
            r.CreateExcelReport(new_temp_path);

            if (Directory.Exists(new_temp_path)) Directory.Delete(new_temp_path,true);

        }

        //Make a correct structure of the path
        private void StructureButton_Click(object sender, EventArgs e)
        {
            string path = PhotoBasePath.Text.ToString();

            if (Directory.Exists(path))
            {
                Photos ph = new Photos();
                ph.StructurePath(path);

                _messageService.ShowMessage("Готово");
            }
            else
            {
                _messageService.ShowExclamation("Указанная папка не существует");
            }
        }

        //Change the global path Base of Photos
        private void GetPhotoBasePath_Click(object sender, EventArgs e)
        {
            string photo_path = Properties.Settings.Default.PhotoPath;

            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                PhotoBasePath.Text = fbd.SelectedPath;
                Properties.Settings.Default.PhotoPath = fbd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void CreateImageReport_Click(object sender, EventArgs e)
        {
            //Collect paths
            string photo_base = PhotoBasePath.Text;
            string input_file = InputPath.Text;
            DateTime dt = dtCalendar.Value;
            bool datetimeoff = dtCalendar.Enabled;
            
            //Check Paths
            if (Directory.Exists(photo_base) && File.Exists(input_file))
            {
                CreatePhotos(photo_base, input_file, dt, datetimeoff);
            }
            else
            {
                _messageService.ShowExclamation("Некорректные входные данные");
            }
        }

        private void butFileOpen_Click(object sender, EventArgs e)
        {
            string path = "D:\\NoReferencePhotoCustoms.txt";
            bool isExist = File.Exists(path);
            if (isExist)
            {
                Process.Start(path);
            }
            else
            {
                _messageService.ShowExclamation("Файл не существует");
            }
        }
        // вкл/выкл дату на фотографиях
        private void checkDateOff_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDateOff.Checked)
            {
                dtCalendar.Enabled = false;
            }
            else
            {
                dtCalendar.Enabled = true;
            }
        }

        private void btGetTechDocSource_Click(object sender, EventArgs e)
        {
            string photo_path = Properties.Settings.Default.OutPathTechDoc;

            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtTechDocSource.Text = fbd.SelectedPath;
                Properties.Settings.Default.OutPathTechDoc = fbd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void btTechDocSpec_Click(object sender, EventArgs e)
        {
            string photo_path = Properties.Settings.Default.InPathTechDoc;

            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtTechDocSpec.Text = fbd.SelectedPath;
                Properties.Settings.Default.InPathTechDoc = fbd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        //Запуск структурирования тех. док. в папку с номером спецификации
        private void btStructureTechDoc_Click(object sender, EventArgs e)
        {
            string input_file = InputPath.Text;
            string out_path = txtTechDocSource.Text;
            string in_path = txtTechDocSpec.Text; 
            
            //Check Paths
            if (File.Exists(input_file) && Directory.Exists(out_path) && Directory.Exists(in_path))
            {
                TechDocuments tech_doc = new TechDocuments();
                tech_doc.StructureTechDoc(input_file, out_path, in_path);

                _messageService.ShowMessage("Готово");
            }
            else
            {
                _messageService.ShowExclamation("Некорректные входные данные");
            }
        }

        //Открыть файл с логом по тех. док
        private void btnOpenTxtNoTechnicalDocumentations_Click(object sender, EventArgs e)
        {
            string path = "D:\\NoTechnicalDocumentations.txt";
            bool isExist = File.Exists(path);
            if (isExist)
            {
                Process.Start(path);
            }
            else
            {
                _messageService.ShowExclamation("Файл не существует");
            }
        }

        //Сброс пользовательских настроек
        private void tsDefaultSettings_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show("Сделать настройки по умолчанию?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                case DialogResult.Yes:

                    Properties.Settings.Default.ChoosePath = Properties.Settings.Default.DefaultChoosePath;
                    InputPath.Text = Properties.Settings.Default.DefaultChoosePath;

                    Properties.Settings.Default.PhotoPath = Properties.Settings.Default.DefaultPhotoPath;
                    PhotoBasePath.Text = Properties.Settings.Default.DefaultPhotoPath;

                    Properties.Settings.Default.OutPathTechDoc = Properties.Settings.Default.DefaultOutPathTechDoc;
                    txtTechDocSource.Text = Properties.Settings.Default.DefaultOutPathTechDoc;

                    Properties.Settings.Default.InPathTechDoc = Properties.Settings.Default.DefaultInPathTechDoc;
                    txtTechDocSpec.Text = Properties.Settings.Default.DefaultInPathTechDoc;

                    Properties.Settings.Default.Save();
                    break;
            }
        }

        //Структурируем фото артикулов по спецификациям
        private void butStructurePhotoBySpecification_Click(object sender, EventArgs e)
        {
            string input_file = InputPath.Text;
            string out_path = PhotoBasePath.Text;
            string in_path = txtTechDocSpec.Text;

            //Check Paths
            if (File.Exists(input_file) && Directory.Exists(out_path) && Directory.Exists(in_path))
            {
                Photos photo = new Photos();
                photo.StructurePhotoBySpecification(input_file, out_path, in_path);

                _messageService.ShowMessage("Готово");
            }
            else
            {
                _messageService.ShowExclamation("Некорректные входные данные");
            }
        }
    }
}
